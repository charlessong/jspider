import hook from "./Observer/hook.js";
import watch from "./Observer/watch.js";
export default { hook, watch };
